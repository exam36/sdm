FROM node

WORKDIR /src

COPY . .
EXPOSE 8000
CMD node Server.js