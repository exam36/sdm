const express = require('express');
const cors = require('cors')


const app = express()
app.use(cors('*'))
app.use(express.json())

const userRouter = require('./routes/User')
app.use('/user',userRouter)

app.listen(8000,'0.0.0.0',()=> {
    console.log('Server started on port 8000')
})
