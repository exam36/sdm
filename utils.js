const createResult=(error,result)=>{
    const Result={};
    if(error){
        Result['status']='Error';
        Result['error']=error;
    }else{
        Result['status']='Success';
        Result['data']=result;
    }
    return Result;
}

const createErrorResult=(error)=>{
    const Result={};
    Result['status']='Error';
    Result['error']=error;
    return Result;
}

const createSuccessResult=(data)=>{
    const Result={};
    Result['status']='Success';
    Result['data']=data;
    return Result;
}
module.exports = {createResult,createErrorResult,createSuccessResult}