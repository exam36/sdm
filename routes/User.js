const { request, response } = require('express');
const express = require('express')
const db = require('../db')
const Result = require('../utils')
const crypt = require('crypto-js')

const app = express.Router();

app.post('/signIn',(request,response)=>{
    console.log("hi hello")
    const {email,password}=request.body;
    const encryptPass = String(crypt.MD5(password))
    const query = `select * from users where email=? and password=?`;
    db.pool.execute(query,[email,encryptPass],(error,users)=>{
        if(error){
            response.send(Result.createErrorResult(error))
        }else if(users.length==0){
            response.send(Result.createErrorResult("User Does Not Exist"))
        }else{
            response.send(Result.createSuccessResult(users[0]))
        }
    })
})
app.post('/signUp',(request,response)=>{
    const {firstname ,lastname, password, email}=request.body;
    const encyptPass = String(crypt.MD5(password))
    const query = `insert into users (firstname ,lastname, password, email) values(?,?,?,?)`;
    db.pool.execute(query,[firstname ,lastname, encyptPass, email],(error,result)=>{
        response.send(Result.createResult(error,result));
    })
})
app.get('/',(req,res)=>{
    const query = 'select * from users'
    db.pool.execute(query,[],(error,result)=>{
        console.log(result);
        res.send(Result.createResult(error,result));
    })
})

module.exports = app;